/**
 * JoystickShield - Arduino Library for JoystickShield (http://hardwarefun.com/projects/joystick-shield)
 *
 * Copyright 2011  Sudar Muthu  (email : sudar@sudarmuthu.com)
 *
 * ----------------------------------------------------------------------------
 * MOD_IRDA Arduino Library for MOD-IRDA+ (https://www.olimex.com/Products/Modules/Interface/MOD-IRDA+/open-source-hardware)
 * 
 * Copyright 2013 OLIMEX LTD/Stefan Mavrodiev (email : support@olimex.com)
 * 
 * ----------------------------------------------------------------------------
 * 
 * For more info about the RC-5 ir codes: https://en.wikipedia.org/wiki/RC-5
 *
 */

#include <JoystickShield.h> // include JoystickShield Library
#include <Wire.h>
#include <IRDA.h>
IRDA irda(0x24);
JoystickShield joystickShield; // create an instance of JoystickShield object

void setup() {
  pinMode(9, OUTPUT);
  irda.setMode(irda.RC5);
  Serial.begin(9600);
  delay(100);
  joystickShield.calibrateJoystick();
}

void loop() {
  joystickShield.processEvents(); // process events

  if (joystickShield.isUp()) {
     Serial.println("Up") ;
     digitalWrite(9, HIGH);
     irda.sendData(0x01, 20);//Up
     delay(300);
  }

  if (joystickShield.isDown()) {
     Serial.println("Down") ;
     digitalWrite(9, HIGH);
     irda.sendData(0x01, 19);//Down
     delay(300);
  }
  
  if (joystickShield.isRight()) {
     Serial.println("Right") ;
     digitalWrite(9, HIGH);
     irda.sendData(0x01, 16);//Volume+
     delay(300);
  }

  if (joystickShield.isLeft()) {
     Serial.println("Left") ;
     digitalWrite(9, HIGH);
     irda.sendData(0x01, 17);//Volume-
     delay(300);
  }

  if (joystickShield.isJoystickButton()) {
     Serial.println("ok") ;
     digitalWrite(9, HIGH);
     irda.sendData(0x01, 53);//ok
     delay(300);
  }

  if (joystickShield.isUpButton()) {
     Serial.println("B2") ;
     digitalWrite(9, HIGH);
     irda.sendData(0x01, 12);//standby
     delay(300);
  }
  delay(100);
  digitalWrite(9, LOW);
}
